//Made by Kacper Feister//


#include "Game.h"
using namespace sf;
using namespace std;


int main()
{
	RenderWindow window(VideoMode(GC::SCREEN_RES.x, GC::SCREEN_RES.y), "Rock Shooter");

	Game game;
	game.Init(window);

	Clock clock;

	while (window.isOpen())
	{
		bool fire = false;

		Event event;
		while (window.pollEvent(event))
		{

			if (event.type == Event::Closed)
				window.close();
			else if (event.type == Event::TextEntered)
			{
				if (event.text.unicode == GC::ESCAPE_KEY)
					window.close();
			}
			else if (event.type == Event::KeyReleased)
			{
				if (event.key.code == Keyboard::Space)
					fire = true;
			}
		}

		window.clear();

		float elapsed = clock.getElapsedTime().asSeconds();
		clock.restart();

		game.Update(window, elapsed, fire);
		game.Render(window, elapsed);

		window.display();
	}

	return EXIT_SUCCESS;
}
