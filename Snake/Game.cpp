//Made by Kacper Feister//
//Tutorial 12 - PointersVectorsStructs


#include <assert.h>
#include <string>
#include <math.h>

#include "Game.h"

using namespace sf;
using namespace std;


//set up background sprite
void Object::InitBackground(RenderWindow& window, Texture& tex)
{
	spr.setTexture(tex);
	IntRect texR(0, 0, 800, 600);
	spr.setTextureRect(texR);
	active = true;
	type = ObjectT::Background;
}

//set up grass sprite
void Object::InitGrass(RenderWindow& window, Texture& tex)
{
	spr.setTexture(tex);
	IntRect texR(0, 0, 800, 300);
	spr.setTextureRect(texR);
	spr.setPosition(0, 300);
	active = true;
	type = ObjectT::Grass;
}

//set up ship sprite
void Object::InitShip(RenderWindow& window, Texture& tex)
{
	spr.setTexture(tex, true);
	const IntRect& texRect = spr.getTextureRect();
	spr.setOrigin(texRect.width / 2.f, texRect.height / 2.f);
	spr.setScale(0.2f, 0.2f);
	spr.setRotation(90);
	spr.setPosition(spr.getGlobalBounds().width * 0.6f, window.getSize().y / 2.f);
	type = ObjectT::Ship;
	radius = 25.f;
	active = true;
}

//set up rock sprite
void Object::InitRock(RenderWindow& window, Texture& tex)
{
	spr.setTexture(tex);
	IntRect texR(0, 0, 96, 96);
	spr.setTextureRect(texR);
	spr.setOrigin(texR.width / 2.f, texR.height / 2.f);
	radius = 10.f + (float)(rand() % 30);
	float scale = 0.75f * (radius / 25.f);
	spr.setScale(scale, scale);
	health = (int)(5 * scale);
	active = false;
	type = ObjectT::Rock;
}

//set up bullet sprite
void Object::InitBullet(RenderWindow& window, Texture& tex)
{
	spr.setTexture(tex);
	IntRect texR(0, 0, 32, 32);
	spr.setTextureRect(texR);
	spr.setOrigin(texR.width / 2.f, texR.height / 2.f);
	radius = 5.f;
	float scale = 0.5f;
	spr.setScale(scale, scale);
	active = false;
	type = ObjectT::Bullet;
}

//set up clouds first sprite
void Object::InitClouds(RenderWindow& window, Texture& tex)
{
	spr.setTexture(tex);
	IntRect texR(0, 0, 800, 600);
	spr.setOrigin(texR.width / 2.f, texR.height / 2.f);
	spr.setScale(1.2f, 1.2f);
	spr.setPosition(100, 300);
	active = true;
	type = ObjectT::Clouds;
}

//set up clouds second sprite
void Object::InitClouds2(RenderWindow& window, Texture& tex)
{
	spr.setTexture(tex);
	IntRect texR(0, 0, 800, 600);
	spr.setOrigin(texR.width / 2.f, texR.height / 2.f);
	spr.setScale(1.2f, 1.2f);
	spr.setPosition(700, 300);
	active = true;
	type = ObjectT::Clouds2;
}

//set up clouds third sprite
void Object::InitClouds3(RenderWindow& window, Texture& tex)
{
	spr.setTexture(tex);
	IntRect texR(0, 0, 800, 600);
	spr.setOrigin(texR.width / 2.f, texR.height / 2.f);
	spr.setScale(1.2f, 1.2f);
	spr.setPosition(1300, 300);
	active = true;
	type = ObjectT::Clouds3;
}

//set up sprite of first background mountains
void Object::InitBackMountains(RenderWindow& window, Texture& tex)
{
	spr.setTexture(tex);
	IntRect texR(0, 0, 800, 600);
	spr.setOrigin(texR.width / 2.f, texR.height / 2.f);
	spr.setScale(2.2f, 2.2f);
	spr.setPosition(700, 630);
	active = true;
	type = ObjectT::Backmountains;
}

//set up sprite of second background mountains
void Object::InitBackMountains2(RenderWindow& window, Texture& tex)
{
	spr.setTexture(tex);
	IntRect texR(0, 0, 800, 600);
	spr.setOrigin(texR.width / 2.f, texR.height / 2.f);
	spr.setScale(2.2f, 2.2f);
	spr.setPosition(50, 630);
	active = true;
	type = ObjectT::Backmountains2;
}

//set up sprite of third background mountains
void Object::InitBackMountains3(RenderWindow& window, Texture& tex)
{
	spr.setTexture(tex);
	IntRect texR(0, 0, 800, 600);
	spr.setOrigin(texR.width / 2.f, texR.height / 2.f);
	spr.setScale(2.2f, 2.2f);
	spr.setPosition(1400, 630);
	active = true;
	type = ObjectT::Backmountains3;
}

//set up first mountain sprite 
void Object::InitMountain1(RenderWindow& window, Texture& tex)
{
	spr.setTexture(tex);
	IntRect texR(0, 0, 800, 600);
	spr.setOrigin(texR.width / 2.f, texR.height / 2.f);
	spr.setScale(1.4f, 1.4f);
	spr.setPosition(100, 600);
	active = true;
	type = ObjectT::Mountain1;

}

//set up second mountain sprite 
void Object::InitMountain2(RenderWindow& window, Texture& tex)
{
	spr.setTexture(tex);
	IntRect texR(0, 0, 800, 600);
	spr.setOrigin(texR.width / 2.f, texR.height / 2.f);
	spr.setScale(1.4f, 1.4f);
	spr.setPosition(700, 600);
	active = true;
	type = ObjectT::Mountain2;
}

//set up third mountain sprite 
void Object::InitMountain3(RenderWindow& window, Texture& tex)
{
	spr.setTexture(tex);
	IntRect texR(0, 0, 800, 600);
	spr.setOrigin(texR.width / 2.f, texR.height / 2.f);
	spr.setScale(1.4f, 1.4f);
	spr.setPosition(1300, 600);
	active = true;
	type = ObjectT::Mountain3;
}

//Initialise objects with their sprites
void Object::Init(RenderWindow& window, Texture& tex, ObjectT type_)
{
	switch (type_)
	{
	case ObjectT::Ship:
		InitShip(window, tex);
		break;
	case ObjectT::Rock:
		InitRock(window, tex);
		break;
	case ObjectT::Bullet:
		InitBullet(window, tex);
		break;
	case ObjectT::Background:
		InitBackground(window, tex);
		break;
	case ObjectT::Clouds:
		InitClouds(window, tex);
		break;
	case ObjectT::Clouds2:
		InitClouds2(window, tex);
		break;
	case ObjectT::Clouds3:
		InitClouds3(window, tex);
		break;
	case ObjectT::Grass:
		InitGrass(window, tex);
		break;
	case ObjectT::Backmountains:
		InitBackMountains(window, tex);
		break;
	case ObjectT::Backmountains2:
		InitBackMountains2(window, tex);
		break;
	case ObjectT::Backmountains3:
		InitBackMountains2(window, tex);
		break;
	case ObjectT::Mountain1:
		InitMountain1(window, tex);
		break;
	case ObjectT::Mountain2:
		InitMountain2(window, tex);
		break;
	case ObjectT::Mountain3:
		InitMountain3(window, tex);
		break;
		assert(false);
	}
}

//update of the objects
void Object::Update(RenderWindow& window, float elapsed, vector<Object>& objects, bool fire)
{
	if (active)
	{
		colliding = false;
		switch (type)
		{
		case ObjectT::Ship:
			PlayerControl(window.getSize(), elapsed, objects, fire);
			break;
		case ObjectT::Rock:
			MoveRock(elapsed);
			break;
		case ObjectT::Bullet:
			MoveBullet(window.getSize(), elapsed);
			break;
		case ObjectT::Clouds:
			MoveClouds(elapsed);
			break;
		case ObjectT::Clouds2:
			MoveClouds2(elapsed);
			break;
		case ObjectT::Clouds3:
			MoveClouds3(elapsed);
			break;
		case ObjectT::Backmountains:
			MoveBackMountains(elapsed);
			break;
		case ObjectT::Backmountains2:
			MoveBackMountains2(elapsed);
			break;
		case ObjectT::Backmountains3:
			MoveBackMountains2(elapsed);
			break;
		case ObjectT::Mountain1:
			MoveMountain1(elapsed);
			break;
		case ObjectT::Mountain2:
			MoveMountain2(elapsed);
			break;
		case ObjectT::Mountain3:
			MoveMountain3(elapsed);
			break;
		}

	}
}

//move rock to the left
//repeat if beyond the screen bounds
void Object::MoveRock(float elapsed)
{
	const Vector2f& pos = spr.getPosition();
	float x = pos.x - GC::ROCK_SPEED * elapsed;
	if (x < -spr.getGlobalBounds().width / 2.f)
		active = false;
	spr.setPosition(x, pos.y);
}

//move bullet to the right
void Object::MoveBullet(const sf::Vector2u& screenSz, float elapsed)
{
	const Vector2f& pos = spr.getPosition();
	float x = pos.x + 250 * elapsed;
	if (x > (screenSz.x + spr.getGlobalBounds().width / 2.f))
		active = false;
	spr.setPosition(x, pos.y);
}

//move 1st sprite of clouds to the left 
//repeat if beyond the screen bounds
void Object::MoveClouds(float elapsed)
{
	const Vector2f& pos = spr.getPosition();
	float x = pos.x - GC::CLOUDS_SPEED * elapsed;
	if (x < -spr.getGlobalBounds().width / 2.f)
		x = 1500;
	spr.setPosition(x, pos.y);
	spr.setPosition(x, pos.y);

}

//move 2nd sprite of clouds to the left 
//repeat if beyond the screen bounds
void Object::MoveClouds2(float elapsed)
{
	const Vector2f& pos = spr.getPosition();
	float x = pos.x - GC::CLOUDS_SPEED * elapsed;
	if (x < -spr.getGlobalBounds().width / 2.f)
		x = 1500;
	spr.setPosition(x, pos.y);
	spr.setPosition(x, pos.y);

}

//move 3rd sprite of clouds to the left 
//repeat if beyond the screen bounds
void Object::MoveClouds3(float elapsed)
{
	const Vector2f& pos = spr.getPosition();
	float x = pos.x - GC::CLOUDS_SPEED * elapsed;
	if (x < -spr.getGlobalBounds().width / 2.f)
		x = 1500;
	spr.setPosition(x, pos.y);
	spr.setPosition(x, pos.y);

}

//move 1st sprite of background mountains to the left 
//repeat if beyond the screen bounds
void Object::MoveBackMountains(float elapsed)
{
	const Vector2f& pos = spr.getPosition();
	float x = pos.x - (GC::BACKMOUNT_SPEED - 10) * elapsed;
	if (x < -spr.getGlobalBounds().width / 2.f)
		x = 2000;
	spr.setPosition(x, pos.y);
	spr.setPosition(x, pos.y);
}

//move 2nd sprite of background mountains to the left 
//repeat if beyond the screen bounds
void Object::MoveBackMountains2(float elapsed)
{
	const Vector2f& pos = spr.getPosition();
	float x = pos.x - (GC::BACKMOUNT_SPEED - 10) * elapsed;
	if (x < -spr.getGlobalBounds().width / 2.f)
		x = 2000;
	spr.setPosition(x, pos.y);
	spr.setPosition(x, pos.y);
}

//move 3rd sprite of background mountains to the left 
//repeat if beyond the screen bounds
void Object::MoveBackMountains3(float elapsed)
{
	const Vector2f& pos = spr.getPosition();
	float x = pos.x - (GC::BACKMOUNT_SPEED - 10) * elapsed;
	if (x < -spr.getGlobalBounds().width / 2.f)
		x = 2000;
	spr.setPosition(x, pos.y);
	spr.setPosition(x, pos.y);
}

//move 1st mountain to the left 
//repeat if beyond the screen bounds
void Object::MoveMountain1(float elapsed)
{
	const Vector2f& pos = spr.getPosition();
	float x = pos.x - GC::MOUNTAIN_SPEED * elapsed;
	if (x < -spr.getGlobalBounds().width / 2.f)
		x = 1250;
	spr.setPosition(x, pos.y);
	spr.setPosition(x, pos.y);
}

//move 2nd mountain to the left 
//repeat if beyond the screen bounds
void Object::MoveMountain2(float elapsed)
{
	const Vector2f& pos = spr.getPosition();
	float x = pos.x - GC::MOUNTAIN_SPEED * elapsed;
	if (x < -spr.getGlobalBounds().width / 2.f)
		x = 1250;
	spr.setPosition(x, pos.y);
	spr.setPosition(x, pos.y);
}

//move 3rd mountain to the left 
//repeat if beyond the screen bounds
void Object::MoveMountain3(float elapsed)
{
	const Vector2f& pos = spr.getPosition();
	float x = pos.x - GC::MOUNTAIN_SPEED * elapsed;
	if (x < -spr.getGlobalBounds().width / 2.f)
		x = 1250;
	spr.setPosition(x, pos.y);
	spr.setPosition(x, pos.y);
}

void Object::Render(RenderWindow& window, float elapsed)
{
	if (active)
		window.draw(spr);
}

//make the ship movement "juicy"
Vector2f Decay(Vector2f& currentVal, float rate, float perSec, float dTimeS)
{
	float mod = 1.0f - rate * (dTimeS / perSec);
	Vector2f alpha(currentVal.x * mod, currentVal.y * mod);
	return alpha;
}


void Object::PlayerControl(const Vector2u& screenSz, float elapsed, std::vector<Object>& objects, bool fire)
{
	Vector2f pos = spr.getPosition();
	const float SPEED = 250.f;
	FloatRect rect = spr.getGlobalBounds();

	static Vector2f thrust{ 0,0 };

	if (Keyboard::isKeyPressed(Keyboard::Up) ||
		Keyboard::isKeyPressed(Keyboard::Down) ||
		Keyboard::isKeyPressed(Keyboard::Left) ||
		Keyboard::isKeyPressed(Keyboard::Right))
	{
		if (Keyboard::isKeyPressed(Keyboard::Up))
			thrust.y = -SPEED;
		else if (Keyboard::isKeyPressed(Keyboard::Down))
			thrust.y = SPEED;
		if (Keyboard::isKeyPressed(Keyboard::Left))
			thrust.x = -SPEED;
		else if (Keyboard::isKeyPressed(Keyboard::Right))
			thrust.x = SPEED;
	}

	pos += thrust * elapsed;
	thrust = Decay(thrust, 0.1f, 0.02f, elapsed);

	if (pos.y < (rect.height * 0.6f))
		pos.y = rect.height * 0.6f;
	if (pos.y > (screenSz.y - rect.height * 0.6f))
		pos.y = screenSz.y - rect.height * 0.6f;
	if (pos.x < (rect.width * 0.6f))
		pos.x = rect.width * 0.6f;
	if (pos.x > (screenSz.x - rect.width * 0.6f))
		pos.x = screenSz.x - rect.width * 0.6f;

	spr.setPosition(pos);

	if (fire)
	{
		Vector2f pos(pos.x + spr.getGlobalBounds().width / 2.f, pos.y);
		FireBullet(pos, objects);
	}
}

void Object::FireBullet(const Vector2f& pos, std::vector<Object>& objects)
{
	size_t idx = 0;
	bool found = false;
	while (idx < objects.size() && !found)
	{
		if (!objects[idx].active && objects[idx].type == ObjectT::Bullet)
			found = true;
		else
			++idx;
	}
	if (idx < objects.size())
	{
		objects[idx].active = true;
		objects[idx].spr.setPosition(pos);
	}
}

//make the ship and bullet objects hittable, rest of them is not
void Object::Hit(Object& other)
{
	switch (type)
	{
	case ObjectT::Ship:
		if (other.type == ObjectT::Rock)
		{
			TakeDamage(1);
			other.TakeDamage(999);
		}
		break;
	case ObjectT::Bullet:
		if (other.type == ObjectT::Rock)
		{
			TakeDamage(1);
			other.TakeDamage(1);
		}
		break;
	case ObjectT::Rock:
		break;
	case ObjectT::Grass:
		break;
	case ObjectT::Background:
		break;
	case ObjectT::Backmountains:
		break;
	case ObjectT::Backmountains2:
		break;
	case ObjectT::Backmountains3:
		break;
	case ObjectT::Clouds:
		break;
	case ObjectT::Clouds2:
		break;
	case ObjectT::Clouds3:
		break;
	case ObjectT::Mountain1:
		break;
	case ObjectT::Mountain2:
		break;
	case ObjectT::Mountain3:
		break;
	default:
		assert(false);
	}
}

void Object::TakeDamage(int amount)
{
	health -= amount;
	if (health <= 0)
		active = false;
}

//load the texture from file
bool LoadTexture(const string& file, Texture& tex)
{
	if (tex.loadFromFile(file))
	{
		tex.setSmooth(true);
		return true;
	}
	assert(false);
	return false;
}

//draw a cricle in order to differenciate which asteroids are hitted, active,
//inactive, out of the screen bounds
void DrawCircle(RenderWindow& window, const Vector2f& pos, float radius, Color col)
{
	CircleShape c;
	c.setRadius(radius);
	c.setPointCount(20);
	c.setOutlineColor(col);
	c.setOutlineThickness(2);
	c.setFillColor(Color::Transparent);
	c.setPosition(pos);
	c.setOrigin(radius, radius);
	window.draw(c);
}

bool CircleToCircle(const Vector2f& pos1, const Vector2f& pos2, float minDist)
{
	float dist = (pos1.x - pos2.x) * (pos1.x - pos2.x) +
		(pos1.y - pos2.y) * (pos1.y - pos2.y);
	dist = sqrtf(dist);
	return dist <= minDist;
}

void CheckCollisions(vector<Object>& objects, RenderWindow& window, bool debug)
{
	if (objects.size() > 1)
	{
		for (size_t i = 0; i < objects.size(); ++i)
		{
			Object& a = objects[i];
			if (a.active)
			{
				if (i < (objects.size() - 1))
					for (size_t ii = i + 1; ii < (objects.size()); ++ii)
					{
						Object& b = objects[ii];
						if (b.active)
						{
							if (CircleToCircle(a.spr.getPosition(), b.spr.getPosition(), a.radius + b.radius))
							{
								a.colliding = true;
								b.colliding = true;
								a.Hit(b);
								b.Hit(a);
							}
						}
					}
				if (debug)
				{
					Color col = Color::Green;
					if (a.colliding)
						col = Color::Red;
					DrawCircle(window, a.spr.getPosition(), a.radius, col);
				}
			}
		}
	}
}

bool IsColliding(Object& obj, vector<Object>& objects)
{
	assert(obj.active);
	size_t idx = 0;
	bool colliding = false;
	while (idx < objects.size() && !colliding) {

		if (&obj != &objects[idx] && objects[idx].active)
		{
			const Vector2f& posA = obj.spr.getPosition();
			const Vector2f& posB = objects[idx].spr.getPosition();
			float dist = obj.radius + objects[idx].radius;
			colliding = CircleToCircle(posA, posB, dist);
		}
		++idx;
	}
	return colliding;
}

//place the rocks in specific order within the screen
void PlaceRocks(RenderWindow& window, Texture& tex, vector<Object>& objects)
{
	bool space = true;
	int ctr = GC::NUM_ROCKS;
	while (space && ctr)
	{
		Object rock;
		rock.Init(window, tex, Object::ObjectT::Rock);
		rock.radius *= GC::ROCK_MIN_DIST;
		int tries = 0;
		do {
			tries++;
			float x = (float)(rand() % window.getSize().x);
			float y = (float)(rand() % window.getSize().y);
			rock.spr.setPosition(x, y);
		} while (tries < GC::PLACE_TRIES && IsColliding(rock, objects));
		rock.radius *= 1 / GC::ROCK_MIN_DIST;
		if (tries != GC::PLACE_TRIES)
			objects.push_back(rock);
		else
			space = false;
		--ctr;
	}
}

bool SpawnRock(RenderWindow& window, vector<Object>& objects, float extraClearance)
{
	size_t idx = 0;
	bool found = false;
	while (idx < objects.size() && !found)
	{
		Object& obj = objects[idx];
		if (!obj.active && obj.type == Object::ObjectT::Rock)
			found = true;
		else
			++idx;
	}

	if (found)
	{
		Object& obj = objects[idx];
		obj.active = true;
		obj.radius += extraClearance;
		FloatRect r = obj.spr.getGlobalBounds();
		float y = (r.height / 2.f) + (rand() % (int)(window.getSize().y - r.height) * 5 / 6);
		obj.spr.setPosition(window.getSize().x + r.width, y);
		if (IsColliding(obj, objects))
		{
			found = false;
			obj.active = false;
		}
		obj.radius -= extraClearance;
	}
	return found;
}

//Initialise objects
//Load files with textures
//Set up the hierarchy of the objects
void Game::Init(sf::RenderWindow& window)
{
	LoadTexture("data/ship.png", texShip);
	LoadTexture("data/asteroid.png", texRock);
	LoadTexture("data/missile-01.png", texBullet);
	LoadTexture("data/mountains01_007.png", texBackground);
	LoadTexture("data/mountains01_006.png", texGrass);
	LoadTexture("data/mountains01_004.png", texClouds);
	LoadTexture("data/mountains01_003.png", texBackMountains);
	LoadTexture("data/mountains01_001.png", texFrontMountain1);
	LoadTexture("data/mountains01_002.png", texFrontMountain2);

	objects.clear();

	//first object, the furtherst one behind
	Object background;
	background.Init(window, texBackground, Object::ObjectT::Background);
	objects.insert(objects.end(), 1, background);

	Object clouds;
	clouds.Init(window, texClouds, Object::ObjectT::Clouds);
	objects.insert(objects.end(), 1, clouds);

	Object clouds2;
	clouds2.Init(window, texClouds, Object::ObjectT::Clouds2);
	objects.insert(objects.end(), 1, clouds2);

	Object clouds3;
	clouds3.Init(window, texClouds, Object::ObjectT::Clouds3);
	objects.insert(objects.end(), 1, clouds3);

	Object backmountains;
	backmountains.Init(window, texBackMountains, Object::ObjectT::Backmountains);
	objects.insert(objects.end(), 1, backmountains);

	Object backmountains2;
	backmountains2.Init(window, texBackMountains, Object::ObjectT::Backmountains2);
	objects.insert(objects.end(), 1, backmountains2);

	Object backmountains3;
	backmountains3.Init(window, texBackMountains, Object::ObjectT::Backmountains3);
	objects.insert(objects.end(), 1, backmountains3);

	Object mountain2;
	mountain2.Init(window, texFrontMountain2, Object::ObjectT::Mountain2);
	objects.insert(objects.end(), 1, mountain2);

	Object mountain3;
	mountain3.Init(window, texFrontMountain2, Object::ObjectT::Mountain3);
	objects.insert(objects.end(), 1, mountain3);

	Object mountain1;
	mountain1.Init(window, texFrontMountain1, Object::ObjectT::Mountain1);
	objects.insert(objects.end(), 1, mountain1);

	Object grass;
	grass.Init(window, texGrass, Object::ObjectT::Grass);
	objects.insert(objects.end(), 1, grass);

	Object rock;
	rock.Init(window, texRock, Object::ObjectT::Rock);
	objects.insert(objects.end(), GC::NUM_ROCKS + 1, rock);

	Object bullet;
	bullet.Init(window, texBullet, Object::ObjectT::Bullet);
	objects.insert(objects.end(), 50, bullet);

	//last object, the furtherst one at the front
	Object ship;
	ship.Init(window, texShip, Object::ObjectT::Ship);
	objects.insert(objects.end(), 1, ship);

	spawnTimer = 0;
	spawnDelay = 0.01f;
	rockShipClearance = objects[GC::NUM_ROCKS].spr.getGlobalBounds().width * 2.f;
	cloudClearance = objects[4].spr.getGlobalBounds().width * 2.f;
}

void Game::Update(sf::RenderWindow& window, float elapsed, bool fire)
{
	spawnTimer += elapsed;
	if (spawnTimer >= spawnDelay)
	{
		if (SpawnRock(window, objects, rockShipClearance))
			spawnTimer = 0;
	}

	CheckCollisions(objects, window, false);
	for (size_t i = 0; i < objects.size(); ++i)
		objects[i].Update(window, elapsed, objects, fire);
}

void Game::Render(sf::RenderWindow& window, float elapsed)
{
	for (size_t i = 0; i < objects.size(); ++i)
		objects[i].Render(window, elapsed);
}
